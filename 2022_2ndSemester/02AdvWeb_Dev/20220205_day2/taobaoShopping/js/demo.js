var vSpan = document.getElementById('version').getElementsByTagName('span');
var cSpan = document.getElementById('color').getElementsByTagName('span');
var rSpan = document.getElementById('rom').getElementsByTagName('span');
var aSpan = document.getElementsByTagName('span');
var oPrice = document.getElementById('price');
var oVersion = document.getElementById('version');
var oRom = document.getElementById('rom');
var numberP = document.getElementById('number');
var totalFee = document.getElementById("totalFee")
vSpan[0].className = 'on';
cSpan[0].className = 'on';
rSpan[0].className = 'on';
var inputEle = numberP.children[1];
inputEle.onchange = function () {
    // inputEle.oninput = function (){
    let totalRepertory = document.getElementById("totalRepertory")
    let curNumber = parseInt(inputEle.value);
    let defaultRepertory = 60;
    //regular expression example
    var reg = /\d{1,8}/;
    if (!reg.test(curNumber)) {
        alert("Please input the number!!!");
        inputEle.value = 1;
        totalRepertory.innerHTML = defaultRepertory;
    }
    if (curNumber > defaultRepertory) {
        alert("Insufficient stock! Please confirm the number is less than or equal " + defaultRepertory);
        inputEle.value = 1;
        totalRepertory.innerHTML = defaultRepertory;
    } else {
        leftNum = defaultRepertory - curNumber + 1;
        if (leftNum >= 0) {
            totalRepertory.innerHTML = leftNum;
            totalFee.innerHTML = parseInt(inputEle.value) * (oPrice.innerHTML);
        }
    }
}

for (var i = 0; i < aSpan.length; i++) {
    //when click every span element
    aSpan[i].onclick = function () {
        // all children of this parent node
        var siblings = this.parentNode.children;
        for (var j = 0; j < siblings.length; j++) {
            //remove the style for all current elements
            siblings[j].className = '';
        }
        //add the style for the element you are clicking now
        this.className = 'on';
        if (this.parentNode == oVersion || this.parentNode == oRom) {
            //execute the function of price() if id is version or rom
            price();
        }
    }
}

function price() {
    var money1 = 0;
    var money2 = 0;
    for (var i = 0; i < vSpan.length; i++) {
        if (vSpan[i].className == 'on') {
            money1 = i ? 5288 : 6088;
            break;
        }
    }
    for (var i = 0; i < rSpan.length; i++) {
        if (rSpan[i].className == 'on') {
            switch (i) {
                case 0:
                    money2 = 0;
                    break;
                case 1:
                    money2 = 800;
                    break;
                case 2:
                    money2 = 1600;
                    break;
            }
        }
    }
    oPrice.innerHTML = money1 + money2;
    totalFee.innerHTML = parseInt(inputEle.value) * (money1 + money2);
}

